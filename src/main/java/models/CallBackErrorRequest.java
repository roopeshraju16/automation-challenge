package models;

public class CallBackErrorRequest {
    private boolean success;
    private long transactionId;
    private String error;

    public CallBackErrorRequest success(boolean success) {
        this.success = success;
        return this;
    }

    public CallBackErrorRequest transactionId(long transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public CallBackErrorRequest error(String error) {
        this.error = error;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public String getError() {
        return error;
    }
}
