package utils;

public class ErrorMessage {
    public static final String ON_BOARD_REQUEST_ERROR="Unable to set the onboard request";

    public static final String ERROR_GENERATE_JWT_TOKEN="Unable to generate the jwt token ";

    public static final String ERROR_GENERATE_PAYER_ID="Unable to get payer Id";

    public static final String UN_ABLE_TO_GET_PAY_BILLS_LIST="Unable to get payer bills";

    public static final String ON_BOARD_SUCCESS_MESSAGE_MISMATCH="onboard success value mismatch";

    public static final String GET_BILLS_MESSAGE_MISMATCH="get paybills api success value mismatch";

    public static final String UN_ABLE_TO_FETCH_ACCOUNT_NUMBER="Unable to fetch account number";

    public static final String UN_ABLE_TO_FETCH_AMOUNT="Unable to fetch amount";

    public static final String UN_ABLE_TO_FETCH_IFSC="Unable to fetch ifsc code";

    public static final String UN_ABLE_TO_MAKE_PAYMENT="Unable to do payment";

    public static final String UN_ABLE_TO_SET_PAYMENT_RESPONSE =
        "Unable to set the payment response to a particular pojo";

    public static final String PAYMENT_SUCCESS_VALUE_MISMATCH = "Payment success value mismatch";

    public static final String ERROR_VALUE_MISMATCH = "error value mismatch";

    public static final String CALL_BACK_URL_ERROR_MESSAGE = "Unable to fetch the call back url response";
}
