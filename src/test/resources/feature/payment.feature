@Payment
Feature: The feature describes all about the payment functionality of a particular bill and provides the notification to the user after every payment is done

  @Positive
  Scenario: Verify with the positive flow by doing the payment for the bill
    Given Onboard a payer
    When Fetch the bills for the particular payer
    And  Pay the bill
    Then Send a call back notification once the payment is done


  @Negative
    Scenario Outline: Verify with the negative flow by doing the payment for the bill
      Given Onboard the payer for to replicate the negative flow
      When Fetch the bills for the payer
      And  Pay the bill by providing the invalid <ifscCode>,<amount>,<accountNumber>
      Then Send a call back notification once the payment is failure
    Examples:
      | ifscCode | amount |accountNumber|
      | *&(!)&(@ |  100   |17819021     |
      |SETU0000  |  100   |1049544037486|


  @Negative
  Scenario: Verify for the error code by providing the invalid token while fetching the bills
    Given Onboard a payer for to replicate invalid token error code in fetching the bills api
    Then Validate the invalid token error code while fetching the bills


  @Negative
  Scenario: Verify for the error code by providing the invalid token while paying the bills
    Given Onboard a payer for to replicate invalid token error code in paying the bills api
    Then Validate the invalid token error code while paying the bills


  @Negative
  Scenario: Verify for the error code by providing an invalid payerId while fetching the bills
    Given Onboard a payer for to replicate the auth error when user provides invalid payId
    Then validate the auth error code while fetching the bills


  @Negative
  Scenario: Verify for the error code by providing an invalid payerId while paying the bills
    Given Onboard a payer for to replicate the auth error when user provides invalid payId in paying the bills
    Then validate the auth error code while paying the bills


  @Negative
  Scenario: Verify for the JWT error code while fetching the bills
    Given Onbard the payer for to replicate the JWT error code
    Then provide the an invalid jwt token while fetching the bills and verify for the jwt token format


  @Negative
  Scenario: Verify for the JWT error code while fetching the bills
    Given Onbard the payer for to replicate the JWT error code during the payments
    Then provide the an invalid jwt token while paying the bills and verify for the jwt token format