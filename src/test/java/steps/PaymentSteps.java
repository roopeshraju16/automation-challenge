package steps;

import api.PaymentService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import models.PayMentResponse;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import utils.ApiPath;
import utils.SchemaPath;


public class PaymentSteps {

    PaymentService paymentService = new PaymentService();

    Response response, payBillsResponse, PaymentResponse,callBackResponse;

    String jwtToken;

    int payerId, amount;

    String accountNumber, ifscCode;

    @Given("^Onboard a payer$") public void onBoardAPayer() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
    }

    @When("^Fetch the bills for the particular payer$")
    public void fetchTheBillsForTheParticularPayer() {
        jwtToken = paymentService.getJwtToken(response, "success", "positiveToken");
        payerId = paymentService.getPayerId(response);
    }

    @And("^Pay the bill$") public void payTheBill() {
        payBillsResponse = paymentService.getPayBills(payerId, jwtToken, 200, "success");
        accountNumber = paymentService.fetchAccountNumber(payBillsResponse);
        amount = paymentService.fetchAmount(payBillsResponse);
        ifscCode = paymentService.fetchIfsc(payBillsResponse);
    }

    @Then("^Send a call back notification once the payment is done$")
    public void sendACallBackNotificationOnceThePaymentIsDone() {
        PaymentResponse =
            paymentService.makePayment(payerId, jwtToken, ifscCode, accountNumber, amount, 200);
        paymentService.validateResponse(PaymentResponse, "success", 200);

        callBackResponse=paymentService.setCallBackRequest(true,
            Long.parseLong(paymentService.getPaymentSuccessValue(PaymentResponse)),
            paymentService.getBillDetails(payBillsResponse));

        paymentService.validateCallBackResponse(callBackResponse,200,"Success");
    }

    @Given("^Onboard a payer for to replicate invalid token error code in fetching the bills api$")
    public void onboardAPayerForToReplicateInvalidTokenErrorCodeInFetchingTheBillsApi() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
        jwtToken = paymentService.getJwtToken(response, "success", "negativeToken");
    }

    @Then("^Validate the invalid token error code while fetching the bills$")
    public void validateTheInvalidTokenErrorCodeWhileFetchingTheBills() {
        payerId = paymentService.getPayerId(response);
        payBillsResponse =
            paymentService.getPayBills(payerId, jwtToken, 400, "The key and token are mandatory!");
    }

    @Given("^Onboard a payer for to replicate invalid token error code in paying the bills api$")
    public void onboardAPayerForToReplicateInvalidTokenErrorCodeInPayingTheBillsApi() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
    }

    @Then("^Validate the invalid token error code while paying the bills$")
    public void validateTheInvalidTokenErrorCodeWhilePayingTheBills() {
        jwtToken = paymentService.getJwtToken(response, "success", "positiveToken");
        payerId = paymentService.getPayerId(response);
        payBillsResponse = paymentService.getPayBills(payerId, jwtToken, 200, "success");
        accountNumber = paymentService.fetchAccountNumber(payBillsResponse);
        amount = paymentService.fetchAmount(payBillsResponse);
        ifscCode = paymentService.fetchIfsc(payBillsResponse);
        PaymentResponse =
            paymentService.makePayment(payerId, "Bearer", ifscCode, accountNumber, amount, 400);
        paymentService.validateResponse(PaymentResponse, "The key and token are mandatory!", 400);
    }

    @Given("^Onboard a payer for to replicate the auth error when user provides invalid payId$")
    public void onboardAPayerForToReplicateTheAuthErrorWhenUserProvidesInvalidPayId() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
        jwtToken = paymentService.getJwtToken(response, "success", "positiveToken");
    }

    @Then("^validate the auth error code while fetching the bills$")
    public void validateTheAuthErrorCodeWhileFetchingTheBills() {
        payerId = paymentService.getPayerId(response);
        payBillsResponse = paymentService.getPayBills(0, jwtToken, 403, "auth error");
    }

    @Given("^Onboard a payer for to replicate the auth error when user provides invalid payId in paying the bills$")
    public void onboardAPayerForToReplicateTheAuthErrorWhenUserProvidesInvalidPayIdInPayingTheBills() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
    }

    @Then("^validate the auth error code while paying the bills$")
    public void validateTheAuthErrorCodeWhilePayingTheBills() {
        jwtToken = paymentService.getJwtToken(response, "success", "positiveToken");
        payerId = paymentService.getPayerId(response);
        payBillsResponse = paymentService.getPayBills(payerId, jwtToken, 200, "success");
        accountNumber = paymentService.fetchAccountNumber(payBillsResponse);
        amount = paymentService.fetchAmount(payBillsResponse);
        ifscCode = paymentService.fetchIfsc(payBillsResponse);
        PaymentResponse =
            paymentService.makePayment(0, jwtToken, ifscCode, accountNumber, amount, 403);
        paymentService.validateResponse(PaymentResponse, "auth error", 403);
    }

    @Given("^Onbard the payer for to replicate the JWT error code$")
    public void onbardThePayerForToReplicateTheJWTErrorCode() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
    }

    @Then("^provide the an invalid jwt token while fetching the bills and verify for the jwt token format$")
    public void provideTheAnInvalidJwtTokenWhileFetchingTheBillsAndVerifyForTheJwtTokenFormat() {
        payerId = paymentService.getPayerId(response);
        payBillsResponse = paymentService
            .getPayBills(payerId, "8901", 400, "The JWT should consist of three parts!");
    }

    @Given("^Onbard the payer for to replicate the JWT error code during the payments$")
    public void onbardThePayerForToReplicateTheJWTErrorCodeDuringThePayments() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
    }

    @Then("^provide the an invalid jwt token while paying the bills and verify for the jwt token format$")
    public void provideTheAnInvalidJwtTokenWhilePayingTheBillsAndVerifyForTheJwtTokenFormat() {
        jwtToken = paymentService.getJwtToken(response, "success", "positiveToken");
        payerId = paymentService.getPayerId(response);
        payBillsResponse = paymentService.getPayBills(payerId, jwtToken, 200, "success");
        accountNumber = paymentService.fetchAccountNumber(payBillsResponse);
        amount = paymentService.fetchAmount(payBillsResponse);
        ifscCode = paymentService.fetchIfsc(payBillsResponse);
        PaymentResponse =
            paymentService.makePayment(payerId, "8901", ifscCode, accountNumber, amount, 400);
        paymentService
            .validateResponse(PaymentResponse, "The JWT should consist of three parts!", 400);
    }

    @Given("^Onboard the payer for to replicate the negative flow$")
    public void onboardThePayerForToReplicateTheNegativeFlow() {
        response = paymentService
            .setOnBoardRequest(SchemaPath.ON_BOARD_POSITIVE, 200, ApiPath.CALL_BACK_API,
                "UltraPayer");
    }

    @When("^Fetch the bills for the payer$") public void fetchTheBillsForThePayer() {
        jwtToken = paymentService.getJwtToken(response, "success", "positiveToken");
        payerId = paymentService.getPayerId(response);
    }

    @And("^Pay the bill by providing the invalid (.*),(.*),(.*)$")
    public void payTheBillWithTheInvalidToken(String ifscCode, int amount, String accountNumber) {
        payBillsResponse = paymentService.getPayBills(payerId, jwtToken, 200, "success");
        PaymentResponse =
            paymentService.makePayment(payerId, jwtToken, ifscCode, accountNumber, amount, 200);
        paymentService.validateResponse(PaymentResponse, "success", 200);
    }

    @Then("^Send a call back notification once the payment is failure$")
    public void sendACallBackNotificationOnceThePaymentIsFailure() {
        callBackResponse = paymentService
            .setCallBackErrorRequest(false, paymentService.getPaymentTransactionId(PaymentResponse),
                "Bill not found");
        paymentService.validateCallBackResponse(callBackResponse,200,"Failed");
    }
}
