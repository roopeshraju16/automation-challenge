package api;

import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import models.*;
import org.junit.Assert;
import utils.ApiPath;
import utils.ErrorMessage;
import utils.JWebToken;

import java.io.File;

import static io.restassured.RestAssured.*;

public class PaymentService {
    private final static String BASE_URI = "http://localhost:3000";

    OnBoardRequest onBoardRequest = new OnBoardRequest();

    OnBoardResponse onBoardResponse = new OnBoardResponse();

    CallBackSuccessRequest callBackSuccessRequest = new CallBackSuccessRequest();

    CallBackErrorRequest callBackErrorRequest = new CallBackErrorRequest();

    CallBackResponse callBackResponse = new CallBackResponse();

    JWebToken jWebToken = new JWebToken();

    Bill bill = new Bill();

    PayBillsResponse PayBillsResponse = new PayBillsResponse();

    PayMentRequest payMentRequest = new PayMentRequest();

    PayMentResponse payMentResponse = new PayMentResponse();

    public Response setOnBoardRequest(String schemaFilePath, int statusCode, String callbackURL,
        String name) {
        Response response = null;
        try {
            onBoardRequest.callbackURL(callbackURL).name(name);
            response = given().header("Accept", "application/json")
                .header("Content-Type", "application/json").body(onBoardRequest).log().all().when()
                .post(BASE_URI + ApiPath.PAYMENT_ON_BOARD).then().assertThat()
                .body(JsonSchemaValidator.matchesJsonSchema(new File(schemaFilePath)))
                .statusCode(statusCode).extract().response();
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.ON_BOARD_REQUEST_ERROR, e);
        }
        return response;
    }


    public String getJwtToken(Response response, String ExpectedMessage, String cases) {
        String token = null;
        try {
            JsonPath responseBody = response.getBody().jsonPath();
            onBoardResponse.setMessage(responseBody.getString("message"));
            Assert.assertEquals(ErrorMessage.ON_BOARD_SUCCESS_MESSAGE_MISMATCH, ExpectedMessage,
                onBoardResponse.getMessage());
            onBoardResponse.setJwtSecret(responseBody.getString("data.jwtSecret"));
            switch (cases) {
                case "positiveToken":
                    token = "Bearer " + jWebToken.getJwtToken(onBoardResponse.getJwtSecret());
                    break;
                case "negativeToken":
                    token = "Bearer";
                    break;
            }
            return token;
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.ERROR_GENERATE_JWT_TOKEN, e);
        }
    }


    public int getPayerId(Response response) {
        try {
            JsonPath responseBody = response.getBody().jsonPath();
            onBoardResponse.setId(responseBody.getInt("data.id"));
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.ERROR_GENERATE_PAYER_ID, e);
        }
        return onBoardResponse.getId();
    }

    public Response getPayBills(int id, String jwtToken, int statusCode, String ExpectedMessage) {
        Response payBillsresponse = null;
        try {
            payBillsresponse = given().header("Accept", "application/json")
                .header("Content-Type", "application/json").header("PAYER-ID", id)
                .header("Authorization", jwtToken).log().all().when()
                .get(BASE_URI + ApiPath.PAYMENT_BILLS).then().assertThat().statusCode(statusCode)
                .extract().response();
            JsonPath responseBody = payBillsresponse.getBody().jsonPath();
            if (statusCode == 200) {
                PayBillsResponse.setMessage(responseBody.getString("message"));
                Assert.assertEquals(ErrorMessage.GET_BILLS_MESSAGE_MISMATCH, ExpectedMessage,
                    PayBillsResponse.getMessage());
            } else {
                PayBillsResponse.setError(responseBody.getString("error"));
                Assert.assertEquals(ErrorMessage.ERROR_VALUE_MISMATCH, ExpectedMessage,
                    PayBillsResponse.getError());
            }
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UN_ABLE_TO_GET_PAY_BILLS_LIST, e);
        }
        return payBillsresponse;
    }

    public String fetchAccountNumber(Response response) {
        String accountNumber = null;
        try {
            if (response != null) {
                JsonPath responseBody = response.getBody().jsonPath();
                PayBillsResponse
                    .setAccountNumber(responseBody.getString("data.bills[0].accountNumber"));
                accountNumber = PayBillsResponse.getAccountNumber();
            }
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UN_ABLE_TO_FETCH_ACCOUNT_NUMBER, e);
        }
        return accountNumber;
    }

    public int fetchAmount(Response response) {
        int amount = 0;
        try {
            if (response != null) {
                JsonPath responseBody = response.getBody().jsonPath();
                PayBillsResponse.setAmount(responseBody.getInt("data.bills[0].amount"));
                amount = PayBillsResponse.getAmount();
            }
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UN_ABLE_TO_FETCH_AMOUNT, e);
        }
        return amount;
    }

    public String fetchIfsc(Response response) {
        String ifsc = null;
        try {
            if (response != null) {
                JsonPath responseBody = response.getBody().jsonPath();
                PayBillsResponse.setAccountNumber(responseBody.getString("data.bills[0].ifsc"));
                ifsc = PayBillsResponse.getAccountNumber();
            }
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UN_ABLE_TO_FETCH_IFSC, e);
        }
        return ifsc;
    }

    public Response makePayment(int id, String jwtToken, String ifsc, String accountNumber,
        int amount, int statusCode) {
        Response makePaymentResponse = null;
        try {
            payMentRequest.accountNumber(accountNumber).amount(amount).ifsc(ifsc);
            makePaymentResponse = given().header("Accept", "application/json")
                .header("Content-Type", "application/json").header("PAYER-ID", id)
                .header("Authorization", jwtToken).body(payMentRequest).log().all()
                .post(BASE_URI + ApiPath.PAYMENT_API).then().assertThat().statusCode(statusCode)
                .extract().response();
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UN_ABLE_TO_MAKE_PAYMENT, e);
        }
        return makePaymentResponse;
    }

    public void validateResponse(Response response, String expectedSuccessValue, int statusCode) {
        try {
            if (response != null) {
                JsonPath responseBody = response.getBody().jsonPath();
                payMentResponse.setMessage(responseBody.getString("message"));
                if (statusCode == 200) {
                    Assert.assertEquals(ErrorMessage.PAYMENT_SUCCESS_VALUE_MISMATCH,
                        expectedSuccessValue, payMentResponse.getMessage());
                } else {
                    payMentResponse.setError(responseBody.getString("error"));
                    Assert.assertEquals(ErrorMessage.ERROR_VALUE_MISMATCH, expectedSuccessValue,
                        payMentResponse.getError());
                }
                payMentResponse.setTransactionId(responseBody.getString("data.transactionId"));
            }
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UN_ABLE_TO_SET_PAYMENT_RESPONSE, e);
        }
    }

    public Response setCallBackRequest(boolean successValue, long transactionId, Bill bill) {
        Response callBackResponse = null;
        try {
            callBackSuccessRequest.success(successValue).transactionId(transactionId).bill(bill);
            callBackResponse = given().header("Accept", "application/json")
                .header("Content-Type", "application/json").body(callBackSuccessRequest).log().all()
                .when().
                    post(ApiPath.CALL_BACK_API).then().assertThat().statusCode(200).extract()
                .response();
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.CALL_BACK_URL_ERROR_MESSAGE, e);
        }
        return callBackResponse;
    }

    public Response setCallBackErrorRequest(boolean successValue, long transactionId,String error) {
        Response callBackResponse = null;
        try {
            callBackErrorRequest.success(successValue).transactionId(transactionId).error(error);
            callBackResponse = given().header("Accept", "application/json")
                .header("Content-Type", "application/json").body(callBackSuccessRequest).log().all()
                .when().
                    post(ApiPath.CALL_BACK_API).then().assertThat().statusCode(200).extract()
                .response();
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.CALL_BACK_URL_ERROR_MESSAGE, e);
        }
        return callBackResponse;
    }



    public String getPaymentSuccessValue(Response response){
        JsonPath responseBody = response.getBody().jsonPath();
        payMentResponse.setTransactionId(responseBody.getString("data.transactionId"));
       return payMentResponse.getTransactionId();
    }

    public Bill getBillDetails(Response response) {
        JsonPath responseBody = response.getBody().jsonPath();
        PayBillsResponse.setId(responseBody.getInt("data.bills[0].id"));
        PayBillsResponse.setBillerName(responseBody.get("data.bills[0].billerName"));
        bill.id(PayBillsResponse.getId()).billerName(PayBillsResponse.getBillerName())
            .amount(fetchAmount(response))
            .accountNumber(fetchAccountNumber(response))
            .ifsc(fetchIfsc(response));
        return bill;
    }

    public void validateCallBackResponse(Response response,int statusCode,String paymentStatus) {
        JsonPath callBackJsonPath = response.getBody().jsonPath();
        callBackResponse.setPaymentStatus(callBackJsonPath.get("paymentStatus"));
        callBackResponse.setStatusCode(callBackJsonPath.get("statusCode"));
        Assert.assertEquals("payment status mismatch", paymentStatus,
            callBackResponse.getPaymentStatus());
        Assert.assertEquals("status code mismatch", statusCode, callBackResponse.getStatusCode());
    }

    public void validateCallBackErrorResponse(Response response,int statusCode,String paymentStatus) {
        JsonPath callBackJsonPath = response.getBody().jsonPath();
        callBackResponse.setPaymentStatus(callBackJsonPath.get("paymentStatus"));
        callBackResponse.setStatusCode(callBackJsonPath.get("statusCode"));
        Assert.assertEquals("payment status mismatch", paymentStatus,
            callBackResponse.getPaymentStatus());
        Assert.assertEquals("status code mismatch", statusCode, callBackResponse.getStatusCode());
    }

    public long getPaymentTransactionId(Response response){
        JsonPath paymentTransaction = response.getBody().jsonPath();
        payMentResponse.setTransactionId(paymentTransaction.getString("data.transactionId"));
        return Long.parseLong(payMentResponse.getTransactionId());
    }
}
