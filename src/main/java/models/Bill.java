package models;

public class Bill {
    private long id;
    private String billerName;
    private int amount;
    private String accountNumber;
    private String ifsc;


    public long getId() {
        return id;
    }

    public String getBillerName() {
        return billerName;
    }

    public int getAmount() {
        return amount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getIfsc() {
        return ifsc;
    }

    public Bill id(long id) {
        this.id = id;
        return this;
    }

    public Bill billerName(String billerName) {
        this.billerName = billerName;
        return this;
    }

    public Bill amount(int amount) {
        this.amount = amount;
        return this;
    }

    public Bill accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public Bill ifsc(String ifsc) {
        this.ifsc = ifsc;
        return this;
    }
}
