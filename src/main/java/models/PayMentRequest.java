package models;

public class PayMentRequest {
    String ifsc;
    String accountNumber;
    int amount;

    public PayMentRequest ifsc(String ifsc) {
        this.ifsc = ifsc;
        return this;
    }

    public PayMentRequest accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public PayMentRequest amount(int amount) {
        this.amount = amount;
        return this;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public int getAmount() {
        return amount;
    }

    public String getIfsc() {
        return ifsc;
    }
}
