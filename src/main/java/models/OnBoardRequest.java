package models;

public class OnBoardRequest {
    String callbackURL;
    String name;

    public OnBoardRequest callbackURL(String callbackURL) {
        this.callbackURL = callbackURL;
        return this;
    }

    public OnBoardRequest name(String name) {
        this.name = name;
        return this;
    }

    public String getCallbackURL() {
        return callbackURL;
    }

    public String getName() {
        return name;
    }
}
