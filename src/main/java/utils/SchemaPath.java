package utils;

import java.io.File;

public class SchemaPath {

    public static final String ON_BOARD_POSITIVE =
        Utility.getSystemDirectory() + File.separator + "src" + File.separator + "test"
            + File.separator + "resources" + File.separator + "schema" + File.separator
            + "onboardpositiveresponse.json";

    public static final String PAY_BILLS_POSITIVE=
        Utility.getSystemDirectory() + File.separator + "src" + File.separator + "test"
            + File.separator + "resources" + File.separator + "schema" + File.separator
            + "paybillspositiveresponse.json";
}
