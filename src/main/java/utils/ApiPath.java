package utils;

public class ApiPath {
    public static final String PAYMENT_ON_BOARD = "/api/onboard";

    public static final String PAYMENT_BILLS = "/api/bills";

    public static final String PAYMENT_API = "/api/pay";

    public static final String CALL_BACK_API="http://localhost:8080/users/notifications";
}
