package models;

public class CallBackSuccessRequest {
    private boolean success;
    private long transactionId;
    private Bill bill;

    public CallBackSuccessRequest success(boolean success) {
        this.success = success;
        return this;
    }

    public CallBackSuccessRequest transactionId(long transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public CallBackSuccessRequest bill(Bill bill) {
        this.bill = bill;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public Bill getBill() {
        return bill;
    }
}
