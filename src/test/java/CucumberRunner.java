import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "/Users/coviam/automation-challenge/src/test/resources/feature/", tags = {"@Payment"}, plugin = {"pretty",
    "html:target/cucumber-html-report", "json:target/cucumber.json",
    "junit:target/cucumber.xml"}, glue = {"steps"}) public class CucumberRunner {
}







