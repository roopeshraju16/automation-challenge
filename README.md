# Tools which had used for to develop the automation suite

1.Maven project 
2.IntellIj editor tool
2.Written in JAVA langauge
3.Implemented with the cucumber-BDD
4.Make sure you should install Cucumber For Java and Gherkin plugin 
5.Rest-assured 

#Soultion 
1. Onboard a payer.
2. Fetch bills.
3. Pay a bill.
4. Sends a callback notification to the registered payer when payments are made. The callback URL is specified while onboarding the payer.

the above four steps will be combined as a flow created the positive and neagtive cases 

#Design 
Test scenario files will be located in test/resources/feature/<featureFile>
  -In the above feature File the test scenarios are completely written in english language
   
Each test step under the scenarios will be mapped to a Test Step class in the form a task/method the class will be located in test/java/steps/<class>
  -In the above class file each step in a particular scenario will be mapped as method 
  
The Rest assure logic and the pojo classes for to construct the request and response payload are completely under the src/main folder  

#scenarios 
All the positive and negative scenarios are mentioned in the payment feature file 


#RunnerClass
->Runner class named as CucumberRunner which is located in src/test/java/CucumberRunner
->Inside that we have an annotation which is @CucumberOptions() which is basically we guide our cucumber to pick the feature and even to pick the scenarios
->Once you had cloned the project please specify the project location of your local file path like the way it mentioned it below
->@CucumberOptions(features = "projectLocation->{/Users/coviam/automation-challenge}/src/test/resources/feature/",
->Test resport will be available in the target/cucumber-html-report/index.html
->You can use the tags which i had mentioned in the feature file
